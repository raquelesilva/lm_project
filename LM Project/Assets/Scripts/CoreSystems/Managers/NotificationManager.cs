using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CoreSystems.Managers
{
	public class NotificationManager : MonoBehaviour
	{
		// ### A dura��o total da anima��o quando este abre/fecha
		[SerializeField] float duration = 1f;

		// ### Tipo de fun��o que ter� um efeito especifico na anima��o de Easing
		[SerializeField] EaseFunctions.Ease easeFunction;

		// ### Refer�ncia do objeto da janela
		[SerializeField] Transform window = null;

		// ### Refer�ncia do objeto que ter� mensagem
		[SerializeField] TextMeshProUGUI message = null;
		
		/// <summary>
		/// Define a mensagem, a cor da mensagem e abre a janela da notifica��o
		/// </summary>
		public void SetMessage(string text, Color messageColor)
		{
			window.gameObject.SetActive(true);
			window.localScale = Vector3.zero;

			message.text = text;
			message.color = messageColor;

			StartCoroutine(nameof(StartMessage));
		}

		/// <summary>
		/// Retorna a dura��o definida
		/// </summary>
		public float GetDuration() 
		{ 
			return duration;
		}

		/// <summary>
		/// Cont�m o processo de anima��o da janela
		/// </summary>
		private IEnumerator StartMessage()
		{
			float currentDuration = 0f;
			float currentPercentage = 0f;

			while(currentPercentage < 2f)
			{
				currentDuration += Time.deltaTime;

				currentPercentage = currentDuration / duration;

				var scaleMode = EaseFunctions.GetEasingFunction(easeFunction);

				float value = scaleMode(0f, 1f, currentPercentage);

				window.localScale = new Vector3(value, value, value);

				yield return null;
			}

			window.gameObject.SetActive(false);
		}
	}
}