using CoreSystems.Extensions.Attributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreSystems.Players
{
	public class Player : MonoBehaviour
	{
		[SerializeField, ShowOnly] string playerName = "PlayerName";

		/// <summary>
		/// Define um novo nome para o personagem
		/// </summary>
		public void SetPlayerName(string playerName)
		{
			this.playerName = playerName;
		}

		/// <summary>
		/// Retorna o nome do personagem
		/// </summary>
		public string GetPlayerName()
		{
			return playerName;
		}

		/// <summary>
		/// Retorna o nome do personagem para ser guardado no ficheiro de grava��o
		/// </summary>
		public object CaptureState()
		{
			return playerName;
		}

		/// <summary>
		/// Carrega o nome do personagem a partir do ficheiro de grava��o
		/// </summary>
		public void RestoreState(object state)
		{
			playerName = (string)state;
		}
	}
}