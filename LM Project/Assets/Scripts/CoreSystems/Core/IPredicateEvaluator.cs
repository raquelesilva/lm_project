using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreSystems.Core
{
    public interface IPredicateEvaluator
    {
        bool? Evaluate(EvaluatorList predicate, string[] parameters);
    }

    public enum EvaluatorList
    {
        Nothing,
        HasInventoryItem,
        IsOnDialogueState,
        HasQuest,
        IsQuestCompleted,
        IsQuestObjectiveCompleted,
        HasMinimumTrait,
        HasItemEquipped,
        HasInputText
    }
}