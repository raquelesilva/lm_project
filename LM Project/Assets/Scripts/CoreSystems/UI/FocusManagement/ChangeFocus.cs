using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CoreSystems.UI.FocusManagement
{
	public class ChangeFocus : MonoBehaviour
	{
		// ### Define se come�a com os objetos dinamicos no inicio do jogo ou n�o
		[SerializeField] bool startAsDynamic = false;

		// ### Lista de objectos UI (User Interface) que podem ser selecionados ao carregar na tecla TAB
		[SerializeField] RectTransform[] rectTransforms = null;

		// ### Lista de objetos UI (User Interface) dinamicos que podem ser selcionados ao carregar na tecla TAB
		[SerializeField] List<RectTransform> dynamicRectTransforms = new List<RectTransform>();

		// ### Index atual do objeto de UI (User Interface) que est� selecionado
		[SerializeField] int currentIndex = 0;

		// ### S� deve de percorrer os objetos dinamicos ou est�ticos
		bool isDynamic = false;

		/// <summary>
		/// Este m�todo � sempre chamado sempre que o jogo inicia
		/// </summary>
		private void Start()
		{
			SetControlDynamic(startAsDynamic);
		}

		/// <summary>
		/// Este m�todo � sempre chamado a cada frame
		/// </summary>
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Tab))
			{
				if (isDynamic)
					SetNextToDynamicFocus();
				else
					SetNextFocus();
			}
		}

		/// <summary>
		/// Define se s� percorre os objetos dinamicos ou est�ticos neste momento
		/// </summary>
		public void SetControlDynamic(bool isDynamic = false)
		{
			currentIndex = -1;

			this.isDynamic = isDynamic;

			if (isDynamic)
			{
				SetNextToDynamicFocus();
				return;
			}

			SetNextFocus();
		}

		/// <summary>
		/// Define os objetos dinamicos
		/// </summary>
		public void SetDynamicRects(IEnumerable<RectTransform> rects)
		{
			if (rects == null)
				return;

			foreach(var rect in rects)
			{
				dynamicRectTransforms.Add(rect);
			}

			EliminateDynamicNullables();
		}

		/// <summary>
		/// Apaga todos os objetos dinamicos definidos anteriormente
		/// </summary>
		public void ResetDynamicRects()
		{
			dynamicRectTransforms.Clear();
		}

		/// <summary>
		/// Seleciona o pr�ximo objeto UI (User Interface) que est� na lista
		/// </summary>
		public void SetNextFocus()
		{
			if (rectTransforms == null || rectTransforms.Length == 0)
				return;
			
			do
			{
				currentIndex++;
				if (currentIndex >= rectTransforms.Length)
					currentIndex = 0;

			} while (!rectTransforms[currentIndex].gameObject.activeSelf);

			if (currentIndex >= rectTransforms.Length) 
				currentIndex = 0;

			var button = rectTransforms[currentIndex].GetComponent<Button>();
			if(button != null)
			{
				button.Select();
				return;
			}

			var inputField = rectTransforms[currentIndex].GetComponent<InputField>();
			if (inputField != null)
			{
				inputField.Select();
				return;
			}

			var inputFieldTextMeshPro = rectTransforms[currentIndex].GetComponent<TMP_InputField>();
			if (inputFieldTextMeshPro != null)
			{
				inputFieldTextMeshPro.Select();
				return;
			}

			var toggle = rectTransforms[currentIndex].GetComponent<Toggle>();
			if (toggle != null)
			{
				toggle.Select();
				return;
			}

			var slider = rectTransforms[currentIndex].GetComponent<Slider>();
			if (slider != null)
			{
				slider.Select();
				return;
			}
		}

		/// <summary>
		/// Percorre os objetos dinamicos com a tecla TAB
		/// </summary>
		public void SetNextToDynamicFocus()
		{
			if (dynamicRectTransforms == null || dynamicRectTransforms.Count == 0)
				return;

			do
			{
				currentIndex++;
				if (currentIndex >= dynamicRectTransforms.Count)
					currentIndex = 0;

			} while (!dynamicRectTransforms[currentIndex].gameObject.activeSelf);

			if (currentIndex >= dynamicRectTransforms.Count)
				currentIndex = 0;

			var button = dynamicRectTransforms[currentIndex].GetComponent<Button>();
			if (button != null)
			{
				button.Select();
				return;
			}

			var inputField = dynamicRectTransforms[currentIndex].GetComponent<InputField>();
			if (inputField != null)
			{
				inputField.Select();
				return;
			}

			var inputFieldTextMeshPro = dynamicRectTransforms[currentIndex].GetComponent<TMP_InputField>();
			if (inputFieldTextMeshPro != null)
			{
				inputFieldTextMeshPro.Select();
				return;
			}

			var toggle = dynamicRectTransforms[currentIndex].GetComponent<Toggle>();
			if (toggle != null)
			{
				toggle.Select();
				return;
			}

			var slider = dynamicRectTransforms[currentIndex].GetComponent<Slider>();
			if (slider != null)
			{
				slider.Select();
				return;
			}
		}

		/// <summary>
		/// Elimina todos os elementos da lista dinamica que estejam com valores null
		/// </summary>
		void EliminateDynamicNullables()
		{
			for(int i = 0; i < dynamicRectTransforms.Count; i++)
			{
				if (i >= dynamicRectTransforms.Count)
					break;

				if (dynamicRectTransforms[i] != null)
					continue;

				dynamicRectTransforms.RemoveAt(i);
			}
		}
	}
}