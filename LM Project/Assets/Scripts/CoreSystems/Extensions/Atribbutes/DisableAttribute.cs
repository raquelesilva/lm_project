using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CoreSystems.Extensions.Attributes
{
	public class DisableAttribute : PropertyAttribute 
	{
		// ### Nome do campo que controlar� a atividade de outros campos que possam estar dependentes
		public string conditionPropertyName;

		// ### Ativa/Desativa atributos no editor com base no nome da condi��o
		public DisableAttribute(string conditionPropertyName)
		{
			this.conditionPropertyName = conditionPropertyName;
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(DisableAttribute))]
	public class DisableDrawer : PropertyDrawer
	{
		/// <summary>
		/// Este m�todo � sempre chamado no Editor de Unity
		/// </summary>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			DisableAttribute disableAttribute = attribute as DisableAttribute;

			// ### Encontra a propriedade que representa o campo com a condi��o
			SerializedProperty conditionProperty = property.serializedObject.FindProperty(disableAttribute.conditionPropertyName);

			// ### Se a propriedade com a condi��o existir e � do tipo correto (boolean)
			if (conditionProperty != null && conditionProperty.propertyType == SerializedPropertyType.Boolean)
			{
				// ### Verifica se a condi��o � falsa e desativa o campo se este verificar correto
				if (!conditionProperty.boolValue)
				{
					EditorGUI.BeginDisabledGroup(true);
				}
			}

			EditorGUI.PropertyField(position, property, label, true);

			if (conditionProperty != null && conditionProperty.propertyType == SerializedPropertyType.Boolean)
			{
				// ### Se a condi��o � falsa, reativa o grupo de campos
				if (!conditionProperty.boolValue)
				{
					EditorGUI.EndDisabledGroup();
				}
			}
		}
	}
#endif
}