using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class ActivateTeleportationRay : MonoBehaviour
{
    public XRInteractorLineVisual leftTeleportation;
    public XRInteractorLineVisual rightTeleportation;

    public InputActionProperty leftActivate;
    public InputActionProperty rightActivate;

    // Update is called once per frame
    void Update()
    {
        leftTeleportation.enabled = leftActivate.action.ReadValue<float>() > 0.1f;
        rightTeleportation.enabled = rightActivate.action.ReadValue<float>() > 0.1f;
    }
}