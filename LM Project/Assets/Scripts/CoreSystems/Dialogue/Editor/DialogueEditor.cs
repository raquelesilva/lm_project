using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace CoreSystems.Dialogues.Editor
{
    public class DialogueEditor : EditorWindow
    {
        // ### Estilo dos nodes
        [NonSerialized] GUIStyle nodeStyle;

        // ### Estilo do texto de di�logo
        [NonSerialized] GUIStyle nodeRichTextSyle;

        // ### Estilo do node principal
        [NonSerialized] GUIStyle rootNodeStyle;

        // ### Estilo do node principal em caso se for o jogador a falar neste node
        [NonSerialized] GUIStyle rootAndPlayerNodeStyle;

        // ### Refer�ncia do node que est� a ser arrastado
        [NonSerialized] DialogueNode draggingNode = null;

        // ### Refer�ncia do node que est� a ser criado
        [NonSerialized] DialogueNode creatingNode = null;

        // ### Refer�ncia do node que est� a ser apagado
        [NonSerialized] DialogueNode deletingNode = null;

        // ### Refer�ncia do node que est� a ser trocado
        [NonSerialized] DialogueNode swapRootNode = null;

        // ### Offset usado quando o node est� a ser arrastado
        [NonSerialized] Vector2 draggingOffset;

        // ### Usado para definir os comportamentos dos bot�es dos nodes parentes
        [NonSerialized] DialogueNode linkingParentNode = null;

        // ### Verifica se o utilizador est� a usar alguma scroll bar para arrastar o canvas do editor
        [NonSerialized] bool draggingCanvas = false;

        // ### Valor de offset usado quando o utilizador est� arrastar o canvas com as scroll bar
        [NonSerialized] Vector2 draggingCanvasOffset;

        // ### Estilo do node de jogador
        [NonSerialized] GUIStyle playerNodeStyle;

        // ### Refer�ncia do di�logo selecionado no Editor, usado para abrir
        Dialogue selectedDialogue = null;

        // ### Valor para fazer os angulos das curvas de Bezier para as linhas que conetam os nodes
        float bezierCurveLinesOffset = 8f;

        // ### Valor da posi��o de scroll bar
        Vector2 scrollPosition;

        // ### Tamanho em pixeis de altura e comprimento de cada tile que constitui a imagem de fundo do editor
        const int backgroundTileSize = 1460;

        // ### Tamanho original do canvas do editor
        private static int canvasSize = 4000;

        // ### Refer�ncia do di�logo que est� aberto atualmente no editor
        private static Dialogue openedDialogue = null;

        /// <summary>
        /// Mostra a janela do Editor
        /// </summary>
        [MenuItem("Window/Dialogue Editor")]
        public static void ShowEditorWindow()
        {
            GetWindow(typeof(DialogueEditor), false, "Dialogue Editor");
        }

        /// <summary>
        /// Abre o Editor quando o utilizador clica no di�logo
        /// </summary>
        [OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            Dialogue dialogue = EditorUtility.InstanceIDToObject(instanceID) as Dialogue;
            openedDialogue = dialogue;
            if (dialogue != null)
            {
                ShowEditorWindow();
                canvasSize = dialogue.GetCanvasSize();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Este m�todo � sempre chamado sempre o Editor fica visivel
        /// </summary>
        private void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChange;

            // Cria o estilo do ret�ngulo do Node
            CreateStyleNode();

            // Cria o estilo do ret�ngulo do Node do jogador
            CreateStylePlayerNode();

            // Cria o estilo do ret�ngulo do Node principal
            CreateStyleRootNode();

            // Cria o estilo do ret�ngulo do Node principal e do jogador
            CreateStyleRootAndPlayerNode();

            // Cria o estilo para o texto dos Nodes
            CreateTextRectangleStyle();
		}

        /// <summary>
        /// Cria o estilo do ret�ngulo que cont�m o texto do Node
        /// </summary>
		private void CreateTextRectangleStyle()
        {
            nodeRichTextSyle = new GUIStyle();
            nodeRichTextSyle.wordWrap = true;
        }

		/// <summary>
		/// Cria o estilo do ret�ngulo do Node principal e do jogador
		/// </summary>
		private void CreateStyleRootAndPlayerNode()
        {
            rootAndPlayerNodeStyle = new GUIStyle();
            rootAndPlayerNodeStyle.normal.background = EditorGUIUtility.Load("node6") as Texture2D;
            rootAndPlayerNodeStyle.normal.textColor = Color.white;
            rootAndPlayerNodeStyle.padding = new RectOffset(20, 20, 20, 20);
            rootAndPlayerNodeStyle.border = new RectOffset(12, 12, 12, 12);
        }

		/// <summary>
		/// Cria o estilo do ret�ngulo do Node principal
		/// </summary>
		private void CreateStyleRootNode()
        {
            rootNodeStyle = new GUIStyle();
            rootNodeStyle.normal.background = EditorGUIUtility.Load("node2") as Texture2D;
            rootNodeStyle.normal.textColor = Color.white;
            rootNodeStyle.padding = new RectOffset(20, 20, 20, 20);
            rootNodeStyle.border = new RectOffset(12, 12, 12, 12);
        }

		/// <summary>
		/// Cria o estilo do ret�ngulo do Node do jogador
		/// </summary>
		private void CreateStylePlayerNode()
        {
            playerNodeStyle = new GUIStyle();
            playerNodeStyle.normal.background = EditorGUIUtility.Load("node1") as Texture2D;
            playerNodeStyle.normal.textColor = Color.white;
            playerNodeStyle.padding = new RectOffset(20, 20, 20, 20);
            playerNodeStyle.border = new RectOffset(12, 12, 12, 12);
        }

		/// <summary>
		/// Cria o estilo do ret�ngulo do Node
		/// </summary>
		private void CreateStyleNode()
        {
            nodeStyle = new GUIStyle();
            nodeStyle.normal.background = EditorGUIUtility.Load("node0") as Texture2D;
            nodeStyle.normal.textColor = Color.white;
            nodeStyle.padding = new RectOffset(20, 20, 20, 20);
            nodeStyle.border = new RectOffset(12, 12, 12, 12);
        }

        /// <summary>
        /// Este m�todo � sempre chamado sempre que o utilizador seleciona outro di�logo
        /// </summary>
        private void OnSelectionChange()
        {
            Dialogue newDialogue = Selection.activeObject as Dialogue;
            if (newDialogue != null)
            {
                selectedDialogue = newDialogue;
                Repaint();
                return;
            }

            newDialogue = openedDialogue;
            if (newDialogue != null)
            {
                selectedDialogue = newDialogue;
                Repaint();
                return;
            }
        }

        /// <summary>
        /// Este m�todo � sempre chamado sempre que o utilizador seleciona a janela do Editor
        /// </summary>
        private void OnFocus()
        {
            OnSelectionChange();
        }

        /// <summary>
        /// Este m�todo � sempre chamado a cada frame do Editor
        /// </summary>
        private void OnGUI()
        {
            if (selectedDialogue == null)
            {
                EditorGUILayout.LabelField("No dialogue selected.");
            }
            else
            {
                ProcessEvents();

                scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

                Rect canvas = GUILayoutUtility.GetRect(canvasSize, canvasSize);

                Texture2D backgroundTex = Resources.Load("background") as Texture2D;

                Rect texCoords = new Rect(0, 0, canvasSize / backgroundTileSize, canvasSize / backgroundTileSize);
                GUI.DrawTextureWithTexCoords(canvas, backgroundTex, texCoords);

                foreach (DialogueNode node in selectedDialogue.GetAllNodes())
                {
                    DrawConnections(node);
                    DrawNode(node);
                }

                EditorGUILayout.EndScrollView();

                if (creatingNode != null)
                {
                    selectedDialogue.CreateNode(creatingNode);
                    creatingNode = null;
                }

                if (deletingNode != null)
                {
                    if (deletingNode.IsRoot())
                    {
                        DialogueNode secondNode = selectedDialogue.GetDialogueNodeByIndex(1);
                        selectedDialogue.SwapRootNode(secondNode);
                        deletingNode.SetRoot(false);
                        secondNode.SetRoot(true);
                    }
                    selectedDialogue.DeleteNode(deletingNode);
                    deletingNode = null;
                }

                if (swapRootNode != null)
                {
                    SwapRootNode(swapRootNode);
                    swapRootNode = null;
                }
            }
        }

        /// <summary>
        /// Processa as a��es do utilizador
        /// </summary>
        private void ProcessEvents()
        {
            if (Event.current.type == EventType.MouseDown && draggingNode == null)
            {
                draggingNode = GetNodeAtPoint(Event.current.mousePosition + scrollPosition);
                if (draggingNode != null)
                {
                    draggingOffset = draggingNode.GetRectPos().position - Event.current.mousePosition;
                    Selection.activeObject = draggingNode;
                }
                else
                {
                    draggingCanvas = true;
                    draggingCanvasOffset = Event.current.mousePosition + scrollPosition;
                    Selection.activeObject = selectedDialogue;
                }

            }
            else if (Event.current.type == EventType.MouseDrag && draggingNode != null)
            {

                draggingNode.SetRectPos(Event.current.mousePosition + draggingOffset);
                GUI.changed = true;
            }
            else if (Event.current.type == EventType.MouseDrag && draggingCanvas)
            {
                scrollPosition = draggingCanvasOffset - Event.current.mousePosition;
                GUI.changed = true;
            }
            else if (Event.current.type == EventType.MouseUp && draggingNode != null)
            {
                draggingNode = null;
            }
            else if (Event.current.type == EventType.MouseUp && draggingCanvas)
            {
                draggingCanvas = false;
            }
        }

        /// <summary>
        /// Desenha os nodes de di�logo
        /// </summary>
        void DrawNode(DialogueNode _node)
        {
            GUIStyle style = nodeStyle;

            if (_node.IsRoot() && _node.IsPlayerSpeaking())
            {
                style = rootAndPlayerNodeStyle;
            }
            else
            {
                if (_node.IsPlayerSpeaking())
                {
                    style = playerNodeStyle;
                }

                if (_node.IsRoot())
                {
                    style = rootNodeStyle;
                }
            }

            GUILayout.BeginArea(_node.GetRectPos(), style);

            GUILayout.BeginVertical();
            UpdateTextRectangleStyle(_node);
            GUILayout.EndVertical();

			GUILayout.BeginVertical();
            DrawSerializedAudio(_node);

            bool hasInput = GUILayout.Toggle(_node.HasInput(), "Has Input?");

            _node.SetHasInput(hasInput);

			GUILayout.EndVertical();

			GUILayout.BeginHorizontal();

            if (GUILayout.Button("-"))
            {
                deletingNode = _node;
            }

            if (GUILayout.Button("+"))
            {
                creatingNode = _node;
            }

            DrawLinkButtons(_node);

            GUILayout.EndHorizontal();


            GUILayout.BeginVertical();

            if (GUILayout.Button("Set Root"))
            {
                swapRootNode = _node;
            }
            GUILayout.EndVertical();

            GUILayout.EndArea();
        }

        /// <summary>
        /// Sempre que o utilizador adiciona ou remove texto da caixa de texto do Node, este atualiza o seu tamanho
        /// </summary>
		private void UpdateTextRectangleStyle(DialogueNode _node)
        {
            int currentHeight = (int)nodeRichTextSyle.CalcHeight(new GUIContent(_node.Text), 200 - 40);
            nodeRichTextSyle.normal.background = new Texture2D(200, currentHeight);
            nodeRichTextSyle.border = new RectOffset(12, 12, 12, 12);

            _node.Text = EditorGUILayout.TextArea(_node.Text, nodeRichTextSyle);

            _node.SetRectSize(200, currentHeight + 120);
        }

        /// <summary>
        /// Desenha o retangulo que cont�m o campo de Audio
        /// </summary>
		private void DrawSerializedAudio(DialogueNode _node)
		{
			SerializedObject serializedObject = new SerializedObject(_node);
			SerializedProperty serializedProperty = serializedObject.FindProperty("voiceAudio");
			serializedProperty.objectReferenceValue =
				EditorGUILayout.ObjectField(serializedProperty.objectReferenceValue, typeof(AudioClip), false);

            _node.VoiceAudio = serializedProperty.objectReferenceValue as AudioClip;
		}

		/// <summary>
		/// Desenha os bot�es que manipulam as liga��es com outros Nodes neste node
		/// </summary>
		void DrawLinkButtons(DialogueNode _node)
        {
            if (linkingParentNode == null)
            {
                if (GUILayout.Button("Link"))
                {
                    linkingParentNode = _node;
                }
            }
            else if (linkingParentNode == _node)
            {
                if (GUILayout.Button("Cancel"))
                {
                    linkingParentNode = null;
                }
            }
            else if (linkingParentNode.GetChildren().Contains(_node.name))
            {
                if (GUILayout.Button("Unlink"))
                {
                    linkingParentNode.RemoveChild(_node.name);
                    linkingParentNode = null;
                }
            }
            else
            {
                if (GUILayout.Button("Child"))
                {
                    linkingParentNode.AddChild(_node.name);
                    linkingParentNode = null;
                }
            }
        }

        /// <summary>
        /// Desenha as linhas bezier de cone��o com os Nodes
        /// </summary>
        void DrawConnections(DialogueNode _node)
        {
            Vector2 startPos = new Vector2(_node.GetRectPos().xMax - bezierCurveLinesOffset, _node.GetRectPos().center.y);
            foreach (DialogueNode childNode in selectedDialogue.GetAllChildren(_node))
            {
                Vector2 endPos = new Vector2(childNode.GetRectPos().xMin + bezierCurveLinesOffset, childNode.GetRectPos().center.y);
                Vector2 controlPointOffset = new Vector2(100, 0);

                Handles.DrawBezier(startPos, endPos, startPos + controlPointOffset, endPos - controlPointOffset, Color.white, null, 4f);
            }
        }

        /// <summary>
        /// Troca algum Node normal pelo o principal
        /// </summary>
        private void SwapRootNode(DialogueNode node)
        {
            foreach (DialogueNode currentNode in selectedDialogue.GetAllNodes())
            {
                if (currentNode == node) continue;

                if (currentNode.IsRoot())
                {
                    currentNode.SetRoot(false);
                }
            }

            node.SetRoot(true);

            if (node != selectedDialogue.GetRootNode())
            {
                selectedDialogue.SwapRootNode(node);
            }
        }

        /// <summary>
        /// Retorna o Node de di�logo que est� numa posi��o especifica no Editor
        /// </summary>
        DialogueNode GetNodeAtPoint(Vector2 _point)
        {
            DialogueNode foundNode = null;
            foreach (DialogueNode node in selectedDialogue.GetAllNodes())
            {
                if (node.GetRectPos().Contains(_point))
                {
                    foundNode = node;
                }
            }

            return foundNode;
        }
    }
}