using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CoreSystems.Dialogues
{
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue/Dialogue", order = 0)]
    public class Dialogue : ScriptableObject, ISerializationCallbackReceiver
    {
        // ### Refer�ncia do nodes que este di�logo possui
        [SerializeField] List<DialogueNode> nodes = new List<DialogueNode>();

        // ### Valor de offset que o novo node dever� de ter quando criado no editor
        [SerializeField] Vector2 newNodeOffset = new Vector2(250, 0);

        // ### Tamanho total original do canvas do Editor
        [SerializeField] int canvasSize = 4000;

        // ### Lista de actors que este dialogo cont�m
		[SerializeField] List<DialogueActor> actors = new List<DialogueActor>();

		// ### Cont�m a lista total de nodes que existem no projeto
		Dictionary<string, DialogueNode> nodeLookUp = new Dictionary<string, DialogueNode>();

        /// <summary>
        /// Este m�todo � sempre chamado sempre que este asset inicia
        /// </summary>
        private void Awake()
        {
            OnValidate();
        }

        /// <summary>
        /// Este fun��o � sempre chamada quando este componente � carregado ou alterado no inspector
        /// </summary>
        private void OnValidate()
        {

            //#if UNITY_EDITOR
            if (nodes.Count <= 1) return;

            nodeLookUp.Clear();
            foreach (DialogueNode node in GetAllNodes())
            {
                nodeLookUp.Add(node.name, node);

                //nodeLookUp[node.name] = node;
            }

            foreach (DialogueNode node in GetAllNodes())
            {
                //nodeLookUp.Add(node.name, node);

                nodeLookUp[node.name] = node;
            }
            //#endif
        }

        /// <summary>
        /// Define os actors do di�logo
        /// </summary>
		public void SetActors(IEnumerable<DialogueActor> newActors)
		{
			actors.Clear();

			foreach (var newActor in newActors)
			{
				actors.Add(newActor);
			}
		}

        /// <summary>
        /// Define o actor principal do di�logo (que dever� de ser o primeiro actor da lista de actors)
        /// </summary>
		public void SetMainActor(DialogueActor newActor)
		{
			if (actors.Count == 0)
			{
				actors.Add(newActor);
				return;
			}
			actors[0] = newActor;
		}

		/// <summary>
		/// Retorna todos os nodes existentes neste di�logo
		/// </summary>
		public IEnumerable<DialogueNode> GetAllNodes()
        {
            return nodes;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Cria um node novo
        /// </summary>
        public void CreateNode(DialogueNode _parentNode)
        {
            DialogueNode newNode = MakeNode(_parentNode);
            Undo.RegisterCreatedObjectUndo(newNode, "Created Dialogue Node");
            Undo.RecordObject(this, "Added Dialogue Node");
            AddNode(newNode);
        }

        /// <summary>
        /// Apaga um node
        /// </summary>
        public void DeleteNode(DialogueNode _nodeToDelete)
        {
            Undo.RecordObject(this, "Removed Dialogue Node");
            nodes.Remove(_nodeToDelete);
            OnValidate();
            CleanDanglingChildren(_nodeToDelete);
            Undo.DestroyObjectImmediate(_nodeToDelete);
        }

        /// <summary>
        /// Retorna o tamanho atual do canvas
        /// </summary>
        public int GetCanvasSize()
        {
            return canvasSize;
        }

        /// <summary>
        /// Na cria��o de um novo node, este define algumas propriedades
        /// </summary>
        private DialogueNode MakeNode(DialogueNode _parentNode)
        {
            DialogueNode newNode = CreateInstance<DialogueNode>();
            newNode.name = Guid.NewGuid().ToString();
            if (_parentNode != null)
            {
                _parentNode.AddChild(newNode.name);
                newNode.SetPlayerSpeaking(!_parentNode.IsPlayerSpeaking());
                newNode.SetRectPos(_parentNode.GetRectPos().position + newNodeOffset);
            }
            else
            {
                newNode.SetRoot(true);
            }

            return newNode;
        }

        /// <summary>
        /// Adiciona o node novo que j� ter� sido criado
        /// </summary>
        private void AddNode(DialogueNode newNode)
        {
            nodes.Add(newNode);
            OnValidate();
        }

        /// <summary>
        /// Quando o node � apaga, remove a cone��o com os seus nodes filhos
        /// </summary>
        void CleanDanglingChildren(DialogueNode _nodeParent)
        {
            foreach (DialogueNode node in GetAllNodes())
            {
                node.RemoveChild(_nodeParent.name);
            }
        }

        /// <summary>
        /// Retorna o Node pelo o seu index na lista
        /// </summary>
        public DialogueNode GetDialogueNodeByIndex(int index)
        {
            if (index >= nodes.Count) return nodes[0];

            return nodes[index];
        }

        /// <summary>
        /// Troca o node pelo o principal
        /// </summary>
        public void SwapRootNode(DialogueNode newNode)
        {
            int newNodeIndex = 0;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (newNode == nodes[i])
                {
                    newNodeIndex = i;
                    break;
                }
            }

            DialogueNode oldNodeRoot = nodes[0];
            nodes[0] = newNode;
            nodes[newNodeIndex] = oldNodeRoot;
        }
#endif
        /// <summary>
        /// Retorna o nome do actor que fala neste Node
        /// </summary>
		public string GetActorName(int actorNumber)
		{
			int actorIndex = actorNumber - 1;
			if (actors[actorIndex] == null || actorIndex >= actors.Count)
			{
				Debug.LogError($"Could not find actor #{actorNumber}.");
				return "Null";
			}
			return actors[actorIndex].GetName();
		}

        /// <summary>
        /// Retorna a imagem do actor que fala neste Node
        /// </summary>
        public Sprite GetActorPortrait(int actorNumber)
        {
			int actorIndex = actorNumber - 1;
			if (actors[actorIndex] == null || actorIndex >= actors.Count)
			{
				Debug.LogError($"Could not find actor #{actorNumber}.");
				return null;
			}
			return actors[actorIndex].GetPortrait();
		}

        /// <summary>
        /// Retorna o n�mero total de actors deste di�logo
        /// </summary>
		public int GetTotalActorsNumber()
		{
			return actors.Count;
		}

		/// <summary>
		/// Retorna o node principal
		/// </summary>
		public DialogueNode GetRootNode()
        {
            return nodes[0];
        }

        /// <summary>
        /// Retorna o ID (ou index na lista) do node
        /// </summary>
        public DialogueNode GetDialogueNodeID(int _ID)
        {
            return nodes[_ID];
        }

        /// <summary>
        /// Retorna todos os nodes filhos deste node
        /// </summary>
        public IEnumerable<DialogueNode> GetAllChildren(DialogueNode _parentNode)
        {
            foreach (string childID in _parentNode.GetChildren())
            {
                if (nodeLookUp.ContainsKey(childID))
                {
                    yield return nodeLookUp[childID];
                }
            }
        }
        
        /// <summary>
        /// Retorna todos os nodes filhos do node do jogador
        /// </summary>
        public IEnumerable<DialogueNode> GetPlayerChildren(DialogueNode _currentNode)
        {
            foreach (DialogueNode _node in GetAllChildren(_currentNode))
            {
                if (_node.IsPlayerSpeaking())
                {
                    yield return _node;
                }
            }
        }

        /// <summary>
        /// Retorna todos os nodes filhos do node do ator
        /// </summary>
        public IEnumerable<DialogueNode> GetAIChildren(DialogueNode _currentNode)
        {
            foreach (DialogueNode _node in GetAllChildren(_currentNode))
            {
                if (!_node.IsPlayerSpeaking())
                {
                    yield return _node;
                }
            }
        }

        /// <summary>
        /// Este m�todo gera um valor GUID de ID quando o di�logo � criado
        /// </summary>
        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            if (nodes.Count <= 0)
            {
                DialogueNode newNode = MakeNode(null);
                AddNode(newNode);
            }

            if (AssetDatabase.GetAssetPath(this) != "")
            {
                foreach (DialogueNode node in GetAllNodes())
                {
                    if (AssetDatabase.GetAssetPath(node) == "")
                    {
                        AssetDatabase.AddObjectToAsset(node, this);
                    }
                }
            }
#endif
        }

		/// <summary>
		/// Este m�todo tem de ser implementado por causa da interface ISerializationCallbackReceiver,
		/// que OnBeforeSerialize() tamb�m usa, n�o tendo outra utilidade, o seu corpo est� vazio
		/// </summary>
		public void OnAfterDeserialize()
        {
        }
    }
}