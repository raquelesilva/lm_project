using CoreSystems.Managers;
using CoreSystems.UI.FocusManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CoreSystems.UI.MessageBox
{
	public class MessageBoxUI : MonoBehaviour
	{
		// ### Desativa todos os bot�es e outros objectos caso esteja visivel 
		[SerializeField] Image backgroundDisabler;

		// ### Utiliza a referencia do objeto que usa como parente para criar os bot�es
		[SerializeField] Transform buttonsContent;

		// ### Usado para a cria��o de bot�es gen�ricos
		[SerializeField] Button buttonPrefab;

		// ### Refer�ncia do bot�o de sa�da
		[SerializeField] Button exitButton;

		// ### Titulo que aparece na janela
		[SerializeField] TextMeshProUGUI titleText;

		// ### Texto do corpo da janela, incluindo a mensagem da mesma
		[SerializeField] TextMeshProUGUI messageText;

		// ### Caixa de texto que permite ao utilizador digitar alguma palavra
		[SerializeField] TMP_InputField inputText;

		// ### Refer�ncia dos bot�es criados quando se abriu a janela
		List<Button> createdButtons = new List<Button>();

		// ### Esta propriedade � uma forma de vericar se alguma message box aberta com este actionID,
		// visto que se for igual, a message box antiga ter� que ser fechada
		string actionID = string.Empty;

		/// <summary>
		/// Este m�todo � sempre chamado sempre que este objeto � destruido
		/// </summary>
		private void OnDestroy()
		{
			var uiManager = FindObjectOfType<UIManager>();
			if (uiManager == null) return;

			uiManager.TryToDisableDynamicOnFocusManager(true);
		}

		#region Setters
		/// <summary>
		/// Abre a caixa de mensagem
		/// </summary>
		public void OpenMessageBox(string titleMessage, string message, ref List<string> buttonsNames, 
			ChangeFocus changeFocus, bool hasBackgroundDisabler = false, string actionID = "", 
			bool hasTextBox = false)
		{
			titleText.text = titleMessage;
			messageText.text = message;

			backgroundDisabler.gameObject.SetActive(hasBackgroundDisabler);

			inputText.gameObject.SetActive(hasTextBox);

			this.actionID = actionID;

			exitButton.onClick.AddListener(() =>
			{
				CloseMessageBox();
			});

			if (buttonsNames == null || buttonsNames.Count == 0)
			{
				CreateTemplateButtons();
				return;
			}

			foreach(var buttonProperties in buttonsNames)
			{
				CreateCustomButtons(buttonProperties);
			}

			var rects = new List<RectTransform>();
			rects.Add(exitButton.GetComponent<RectTransform>());
			rects.Add(inputText.GetComponent<RectTransform>());
			foreach(var createdButton in createdButtons)
			{
				rects.Add(createdButton.GetComponent<RectTransform>());
			}
			changeFocus.SetDynamicRects(rects);
			changeFocus.SetControlDynamic(true);
		}

		/// <summary>
		/// Fecha a Message Box, se for apenas para esconder, s� desativa, sen�o destroi a janela
		/// </summary>
		public void CloseMessageBox(bool toHide = false)
		{
			if (toHide)
			{
				gameObject.SetActive(false);
				return;
			}

			Destroy(gameObject);
		}
		#endregion

		#region Getters
		/// <summary>
		/// Retorna o action ID para efeitos de gest�o de message boxes
		/// </summary>
		public string GetActionID()
		{
			return actionID;
		}

		/// <summary>
		/// Obt�m a refer�ncia do bot�o pela a posi��o na lista
		/// </summary>
		public Button GetButtonByCreatedID(int id)
		{
			if (!DoesButtonExists(id)) return null;
			return createdButtons[id];
		}

		/// <summary>
		/// Obt�m a refer�ncia do bot�o pelo o texto
		/// </summary>
		public Button GetButtonByText(string text)
		{
			if (!DoesButtonExists(text)) return null;

			foreach (var createdButton in createdButtons)
			{
				if (createdButton == null) continue;

				var buttonText = createdButton.GetComponentInChildren<TextMeshProUGUI>();
				if (buttonText == null) continue;

				if (buttonText.text != text) continue;

				return createdButton;
			}

			return null;
		}

		/// <summary>
		/// Retorna a caixa de texto
		/// </summary>
		public TMP_InputField GetInputText()
		{
			return inputText;
		}
		#endregion

		#region Private
		/// <summary>
		/// Quando o utilizador n�o especifica os bot�es pretendidos, o sistema tenta criar bot�es pr�-definidos
		/// </summary>
		void CreateTemplateButtons()
		{
			var buttonOk = Instantiate(buttonPrefab, buttonsContent);

			var buttonText = buttonOk.GetComponentInChildren<TextMeshProUGUI>();
			if (buttonText != null) buttonText.text = "Ok";

			buttonOk.onClick.AddListener(() =>
			{
				Destroy(gameObject);
			});

			var buttonCancel = Instantiate(buttonPrefab, buttonsContent);
			buttonText = buttonCancel.GetComponentInChildren<TextMeshProUGUI>();
			if (buttonText != null) buttonText.text = "Cancel";

			buttonCancel.onClick.AddListener(() => 
			{
				Destroy(gameObject);
			});
		}

		/// <summary>
		/// Quando o utilizador especifica os bot�es pretendidos, o sistema tenta criar os mesmos
		/// </summary>
		void CreateCustomButtons(string buttonName)
		{
			var button = Instantiate(buttonPrefab, buttonsContent);
			var buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
			if (buttonText != null) buttonText.text = buttonName;
			createdButtons.Add(button);
		}

		/// <summary>
		/// Verifica se o bot�o costumiz�vel existe ou n�o, pela a posi��o da lista createdButtons
		/// </summary>
		bool DoesButtonExists(int id)
		{
			if (createdButtons == null || createdButtons.Count == 0)
			{
				Debug.LogWarning($"Nenhum bot�o costumiz�vel fora criado nesta janela {gameObject.name}.");

				return false;
			}

			if (createdButtons.Count <= id)
			{
				Debug.LogWarning($"O bot�o costumiz�vel pretendido pelo o ID: {id} n�o existe nesta janela {gameObject.name}.");

				return false;
			}

			return true;
		}

		/// <summary>
		/// Verifica se o bot�o costumiz�vel existe ou n�o, pelo o texto usado no mesmo
		/// </summary>
		bool DoesButtonExists(string text)
		{
			foreach(var createdButton in createdButtons)
			{
				if (createdButton == null) continue;

				var buttonText = createdButton.GetComponentInChildren<TextMeshProUGUI>();
				if (buttonText == null) continue;

				if (buttonText.text != text) continue;

				return true;
			}

			return false;
		}
		#endregion
	}
}