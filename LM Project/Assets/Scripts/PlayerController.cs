using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit;

namespace RPG.Control
{
    public class PlayerController : MonoBehaviour
    {
        public static PlayerController Instance;

        [SerializeField]
        Camera cam;
        [SerializeField]
        NavMeshAgent navMeshAgent;

        [SerializeField]
        LayerMask IgnoreLayer;

        [SerializeField]
        GameObject cursorAnimObject;

        GameObject newCursor;

        bool isSprinting = false;

        [SerializeField]
        bool isPaused = false;

        [SerializeField] private float rotateSpeed = 100.0f;
        [SerializeField] float sprintSpeed = 3f;

        Vector3 moveDestination;

        // ### WASD Movement
        private float movementSpeed;
        private float directionForward = 0f;
        private Vector3 previousPosition;
        private Vector3 v_Movement;

        bool canMouse = true;

        public float MovementSpeed
        {
            get
            {
                return movementSpeed;
            }
        }
        private void Awake()
        {
            Instance = this;

            isPaused = false;
            canMouse = true;

            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        void Update()
        {
            //InputMouse_OrbitCamera();

            if (isPaused) return;

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) ||
                Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
            {
                navMeshAgent.ResetPath();
            }

            // ### WASD Controls
            if (Input.GetKey(KeyCode.LeftShift))
            {
                isSprinting = true;
            }
            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                isSprinting = false;
            }

            if (Input.GetKey(KeyCode.W))
            {
                MoveForward();
            }
            else if (Input.GetKey(KeyCode.S))
            {
                MoveBackwards();
            }

            if (Input.GetKey(KeyCode.A))
            {
                TurnLeft();
            }
            else if (Input.GetKey(KeyCode.D))
            {
                TurnRight();
            }

            //if (raycastControl.InteractWithUI()) return;
            //if (raycastControl.InteractWithComponent(this)) return;
            if (canMouse && Input.GetMouseButtonUp(0))
            {
                MovePlayerToPoint();
                return;
            }
        }

        private void FixedUpdate()
        {
            ProcessMovement();
        }

        // ### WASD Controls
        void ProcessMovement()
        {
            v_Movement = (cam.transform.forward * directionForward).normalized;
            movementSpeed = ((transform.position - previousPosition).magnitude) / Time.fixedDeltaTime;
            previousPosition = transform.position;
        }

        public void MoveForward()
        {
            if (isPaused) return;
            directionForward = 1f;
            Vector3 destinationPos = transform.position + v_Movement * (navMeshAgent.speed) * Time.deltaTime;

            if (isSprinting) destinationPos = transform.position + v_Movement * (navMeshAgent.speed) * Time.deltaTime * sprintSpeed;

            transform.position = Vector3.MoveTowards(transform.position, destinationPos, 1f);
        }

        public void MoveBackwards()
        {
            if (isPaused) return;
            if (cam == null) return;
            directionForward = -1f;
            Vector3 destinationPos = transform.position + v_Movement * (navMeshAgent.speed / 2) * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, destinationPos, 1f);
        }

        public void TurnLeft()
        {
            if (isPaused) return;
            transform.Rotate((Vector3.up * -1.0f) * rotateSpeed * Time.deltaTime);
            //cam.transform.rotation = transform.rotation;
            RotateInstant();
        }

        public void TurnRight()
        {
            if (isPaused) return;
            transform.Rotate((Vector3.up * 1.0f) * rotateSpeed * Time.deltaTime);
            //cam.transform.rotation = transform.rotation;
            RotateInstant();
        }

        public void RotateInstant()
        {
            if (isPaused) return;
            // Rotate instantly when changing direction
            if (navMeshAgent.velocity.sqrMagnitude > Mathf.Epsilon)
            {
                transform.rotation = Quaternion.LookRotation(navMeshAgent.velocity.normalized);
            }
        }

        static public bool IsAgentOnNavMesh(GameObject agentObject, float onMeshThreshold)
        {
            Vector3 agentPosition = agentObject.transform.position;
            NavMeshHit hit;

            // Check for nearest point on navmesh to agent, within onMeshThreshold
            if (NavMesh.SamplePosition(agentPosition, out hit, onMeshThreshold, NavMesh.AllAreas))
            {
                // Check if the positions are vertically aligned
                if (Mathf.Approximately(agentPosition.x, hit.position.x)
                    && Mathf.Approximately(agentPosition.z, hit.position.z))
                {
                    // Lastly, check if object is below navmesh
                    return agentPosition.y >= hit.position.y;
                }
            }

            return false;
        }

        private void MovePlayerToPoint()
        {
            if (cam == null)
            {
                return;
            }

            if (EventSystem.current.IsPointerOverGameObject()) return;

            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue, 0.5f);

            if (Physics.Raycast(ray, out hit, 100, ~IgnoreLayer))
            {
                /*else
                {
                    raycastControl.InteractMovable(true);
                    RemoveFocus();
                    MoveToPoint(hit.point);
                    camMove.ResetLookOnTarget();
                }*/
            }

            /*NavMeshPath path = new NavMeshPath();
            navMeshAgent.CalculatePath(hit.point, path);
            if (path.status == NavMeshPathStatus.PathPartial)
            {
                return;
            }

            navMeshAgent.SetDestination(hit.point);*/
        }

        public void MoveToPoint(Vector3 point)
        {
            if (!destinationIsReachable(transform.position, point))
            {
                return;
            }
            NavMeshPath path = new NavMeshPath();
            navMeshAgent.CalculatePath(point, path);
            if (path.status == NavMeshPathStatus.PathPartial)
            {
                return;
            }

            newCursor = Instantiate(cursorAnimObject, point, Quaternion.identity);
            navMeshAgent.SetDestination(point);

            moveDestination = point;

            Destroy(newCursor, 5f);
        }

        private bool destinationIsReachable(Vector3 position, Vector3 destination)
        {
            NavMeshPath meshPath = new NavMeshPath();
            NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, meshPath);
            return meshPath.status != NavMeshPathStatus.PathInvalid;
        }

        public bool IsPaused()
        {
            return isPaused;
        }

        public void SetPause(bool b)
        {
            isPaused = b;

            GetComponent<ContinuousMoveProviderBase>().moveSpeed = b ? 0 : 5;
            GetComponent<ContinuousTurnProviderBase>().turnSpeed = b ? 0 : 60;
        }

        public void ToggleIsPause()
        {
            isPaused = !isPaused;
        }

        public void SetMouse(bool b)
        {
            canMouse = b;
        }

        public void WarpPlayer(Transform chosenPosition)
        {
            //navMeshAgent.enabled = false;
            this.gameObject.transform.position = chosenPosition.position;
            this.gameObject.transform.rotation = chosenPosition.localRotation;
            //navMeshAgent.enabled = true;
        }

        public float GetRotateSpeed()
        {
            return rotateSpeed;
        }
    }
}