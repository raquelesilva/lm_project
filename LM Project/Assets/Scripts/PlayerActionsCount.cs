using CoreSystems.Dialogues;
using RPG.Control;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionsCount : MonoBehaviour
{
    public int reputation;
    public AIConversant playMiddleScene;
    public AIConversant playLastScene;
    public Dialogue middleGoodDialogue;
    public Dialogue middleNeutralDialogue;
    public Dialogue middleBadDialogue;
    public Dialogue lastGoodDialogue;
    public Dialogue lastNeutralDialogue;
    public Dialogue lastBadDialogue;

    public PlayerController playerController;
    
    public Transform goodEndingPlace;
    public Transform neutralEndingPlace;
    public Transform badEndingPlace;

    public GameObject butterflies;
    public GameObject wasps;

    public Color lightBlue;
    public Color darkBlue;


    public void ChangeReputation(int value)
    {
        reputation += value;
    }

    public void GetLastScene()
    {
        Debug.Log("GetLastScene: " + reputation);
        //playLastScene.SetDialogueState("startDialogue");

        if (reputation > 0)
        {
            playLastScene.SetDialogue(lastGoodDialogue);
            playerController.WarpPlayer(goodEndingPlace);
            butterflies.SetActive(true);
        }

        if (reputation == 0)
        {
            playLastScene.SetDialogue(lastNeutralDialogue);
            playerController.WarpPlayer(neutralEndingPlace);
        }

        if(reputation < 0)
        {
            playLastScene.SetDialogue(lastBadDialogue);
            playerController.WarpPlayer(badEndingPlace);
            wasps.SetActive(true);
        }

        playLastScene.StartDialogue();
    }

    public void GetMiddleScene()
    {
        if (reputation > 0)
        {
            playMiddleScene.SetDialogue(middleGoodDialogue);
        }

        if (reputation == 0)
        {
            playMiddleScene.SetDialogue(middleNeutralDialogue);
        }

        if (reputation < 0)
        {
            playMiddleScene.SetDialogue(middleBadDialogue);
        }

        playMiddleScene.StartDialogue();
    }

    public void ChangeToBadWeather()
    {
        Camera.main.backgroundColor = darkBlue;
    }

    public void ChangeToGoodWeather()
    {
        Camera.main.backgroundColor = lightBlue;
    }
}