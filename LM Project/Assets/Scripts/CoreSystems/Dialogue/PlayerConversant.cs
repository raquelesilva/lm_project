using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using CoreSystems.Core;
using UnityEngine.SceneManagement;

namespace CoreSystems.Dialogues
{
	public class PlayerConversant : MonoBehaviour, IPredicateEvaluator
    {
		// ### Refer�ncia do componente do AudioSource
		[SerializeField] AudioSource audioSource;

		// ### O asset de di�logo atual
		Dialogue currentDialogue;

		// ### Cont�m o texto, nome do ator e a imagem do di�logo
		DialogueNode currentNode = null;

		// ### Refer�ncia do componente AIConversant do ator que o jogador est� a interagir
		AIConversant currentConversant = null;

		// ### Este valor booleano permite verificar se o jogador tem que selecionar alguma op��o de resposta
		bool isChoosing = false;

		// ### Resposta atual do jogador
		string answer;

		// ### Sempre que di�logo avan�a (ou sai) este evento � sempre chamado
		public event Action OnConversationUpdate;

		[HideInInspector] private bool darkenBackground = true;
		[HideInInspector] private bool playerCanQuitDialog = true;

		/// <summary>
		/// Este m�todo � sempre chamado sempre que o jogo inicia
		/// </summary>
		private void Awake()
		{
		}

        private void Start()
        {
            
        }

        private void OnEnable()
		{
			SceneManager.sceneLoaded += OnActiveSceneChange;
		}

		private void OnDisable()
		{
			SceneManager.sceneLoaded -= OnActiveSceneChange;
		}

		private void OnActiveSceneChange(Scene arg0, LoadSceneMode arg1)
		{
			currentConversant = null;
			currentDialogue = null;
			currentNode = null;
			darkenBackground = false;
            isChoosing = false;
			
			if (OnConversationUpdate != null) OnConversationUpdate();
		}

		/// <summary>
		/// Este m�todo configura os it�ns de di�logo e come�a o mesmo
		/// </summary>
		public void StartDialogue(AIConversant _newConversant, Dialogue _dialogue, bool needsToDarkenBackGround, bool canQuitDialog, int _startDialogID = 0)
		{
			if (IsActive()) return;
			Debug.Log("Is active");
			currentConversant = _newConversant;
			currentDialogue = _dialogue;

			if (_startDialogID <= 0)
			{
				Debug.Log("StartDialog: " + _startDialogID);
				currentNode = currentDialogue.GetRootNode();
			}
			else
			{
				currentNode = currentDialogue.GetDialogueNodeID(_startDialogID);
				Debug.Log("currentNode: " + currentNode);
			}

			audioSource.clip = currentNode.VoiceAudio;
			if (audioSource.clip != null) audioSource.Play();
			Debug.Log("aUDIOsOURCE: " + audioSource);

			TriggerEnterAction();
            darkenBackground = needsToDarkenBackGround;
			playerCanQuitDialog = canQuitDialog;
			OnConversationUpdate?.Invoke();
		}

		/// <summary>
		/// Avan�a no di�logo
		/// </summary>
		public void Next()
		{
			audioSource.clip = null;

			if (!HasNext())
			{
				Quit();
				return;
			}

			int numberOfPlayerResponses = FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode)).Count();

			//isChoosing = false;
			if (numberOfPlayerResponses > 1)
			{
				isChoosing = true;
				TriggerExitAction();
				OnConversationUpdate.Invoke();
				return;
			}
			else if (numberOfPlayerResponses == 1)
			{
				//DialogueNode child = currentDialogue.GetPlayerChildren(currentNode).ToArray()[0];
				DialogueNode child = FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode)).ToArray()[0];
				TriggerExitAction();
				currentNode = child;
				TriggerEnterAction();

				audioSource.clip = currentNode.VoiceAudio;
				if (audioSource.clip != null) audioSource.Play();

				OnConversationUpdate?.Invoke();
				return;
			}
			else
			{
				isChoosing = false;
			}

			DialogueNode[] children = FilterOnCondition(currentDialogue.GetAIChildren(currentNode)).ToArray();
			int randomIndex = UnityEngine.Random.Range(0, children.Count());

			if (children.Length < randomIndex || children.Length == 0)
			{
				Quit();
			}
			else
			{
				TriggerExitAction();
				currentNode = children[randomIndex];
			}

			audioSource.clip = currentNode.VoiceAudio;
			if (audioSource.clip != null) audioSource.Play();

			TriggerEnterAction();
			OnConversationUpdate?.Invoke();
		}

		/// <summary>
		/// Este m�todo � sempre chamado sempre que o jogador seleciona uma resposta
		/// </summary>
		public void SelectChoice(DialogueNode _dialogueNode)
		{
			currentNode = _dialogueNode;
			TriggerEnterAction();
			isChoosing = false;
			Next();
		}

		/// <summary>
		/// Este m�todo permite o jogador sair do di�logo
		/// </summary>
		public void Quit()
		{
			TriggerExitAction();
			FinishDialogue();
			currentConversant = null;
			currentDialogue = null;
			currentNode = null;
			isChoosing = false;
			if (OnConversationUpdate != null) OnConversationUpdate();
		}

		/// <summary>
		/// Define a resposta atual do jogador
		/// </summary>
		public void SetAnswer(string answer)
		{
			this.answer = answer;
		}

		/// <summary>
		/// Verifica se o di�logo est� ativo
		/// </summary>
		public bool IsActive()
		{
			return currentDialogue != null;
		}

        public bool NeedAdjustmentLayer()
        {
			return darkenBackground;
        }
        public bool CanQuitDialog()
        {
			Debug.Log("CanQuitDialog: " + playerCanQuitDialog);
			return playerCanQuitDialog;
        }
        /// <summary>
        /// Retorna o nome do ator que est� a falar neste momento
        /// </summary>
        public string GetCurrentConversantName()
		{
			if (isChoosing || currentNode.IsPlayerSpeaking())
			{
				//Adicionar Novamente depois
				//return player.GetPlayerName();
				return "Eu";
			}

			if (currentDialogue.GetTotalActorsNumber() > 0)
				return currentDialogue.GetActorName(currentNode.GetActorOverride());

			return currentConversant.GetName();
		}
		
		/// <summary>
		/// Retorna a imagem do actor que est� a falar neste momento
		/// </summary>
		public Sprite GetCurrentConversantPortrait()
		{
			if (isChoosing || currentNode.IsPlayerSpeaking())
			{
				return null;
			}

			if (currentDialogue.GetTotalActorsNumber() > 0)
				return currentDialogue.GetActorPortrait(currentNode.GetActorOverride());

			return null;
		} 

		/// <summary>
		/// Retorna o texto de di�logo
		/// </summary>
		public string GetText()
		{
			if (currentNode == null)
			{
				return "Hello.";
			}

			return currentNode.Text;
		}

		/// <summary>
		/// Retorna a resposta, caso o jogador tenha que dar resposta neste di�logo
		/// </summary>
		public string GetAnswer()
		{
			return answer;
		}

		/// <summary>
		/// Retorna o audio da fala do nodo
		/// </summary>
		public AudioClip GetAudio()
		{
			if (currentNode == null)
			{
				return null;
			}

			return currentNode.VoiceAudio;
		}

		/// <summary>
		/// Retorna as escolhas que o jogador poder� ter para dar a resposta
		/// </summary>
		public IEnumerable<DialogueNode> GetChoices()
		{
			return FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode));
		}

		/// <summary>
		/// Verifica se o jogador j� chegou ao fim do di�logo
		/// </summary>
		public bool HasNext()
		{
			return FilterOnCondition(currentDialogue.GetAllChildren(currentNode)).Count() > 0;
		}

		/// <summary>
		/// Filtra os di�logos com base em condi��es
		/// </summary>
		private IEnumerable<DialogueNode> FilterOnCondition(IEnumerable<DialogueNode> inputNode)
		{
			foreach (var node in inputNode)
			{
				Debug.Log("Checknode: " + node.CheckCondition(GetEvaluators()));
				if (node.CheckCondition(GetEvaluators()) && node.CheckCondition(GetEvaluators()))
				{
					yield return node;
				}
			}
		}

		/// <summary>
		/// Verifica se o jogador tem de escolher uma op��o para dar uma resposta
		/// </summary>
		public bool IsChoosing()
		{
			return isChoosing;
		}

		/// <summary>
		/// Verfica se este di�logo tem alguma caixa de texto para resposta
		/// </summary>
		public bool HasInput()
		{
			return currentNode.HasInput();
		}

		/// <summary>
		/// Retorna todos os resultados de condi��o de todos os componentes que implementem a interface IPredicateEvaluator
		/// </summary>
		private IEnumerable<IPredicateEvaluator> GetEvaluators()
		{
			return GetComponents<IPredicateEvaluator>();
		}

		/// <summary>
		/// Se o jogador chegou a um di�logo que tenha alguma a��o, este ativa essa a��o no DialogueTrigger quando entra
		/// </summary>
		private void TriggerEnterAction()
		{
			if (currentNode != null)
			{
				TriggerAction(currentNode.GetOnEnterAction());
				Debug.Log("TriggerAction: " + currentNode.GetOnEnterAction());
			}
		}

		/// <summary>
		/// Se o jogador chegou a um di�logo que tenha alguma a��o, este ativa essa a��o no DialogueTrigger quando sai
		/// </summary>
		private void TriggerExitAction()
		{
			if (currentNode != null)
			{
				TriggerAction(currentNode.GetOnExitAction());
			}
		}

		/// <summary>
		/// Ativa a a��o
		/// </summary>
		private void TriggerAction(string action)
		{
			Debug.Log("Action: " + action);
			if (action == "") return;

			foreach (DialogueTrigger trigger in currentConversant.GetComponents<DialogueTrigger>())
			{
				Debug.Log("triggerAction: " + trigger);
				trigger.Trigger(action);
			}
		}

		/// <summary>
		/// Executa os eventos do final de di�logo quando o jogador fecha ou quando este � terminado
		/// </summary>
		private void FinishDialogue()
		{
			if (currentConversant == null)
				return;

			foreach (DialogueTrigger trigger in currentConversant.GetComponents<DialogueTrigger>())
			{
				trigger.FinishDialogue();
			}
		}

        public bool? Evaluate(EvaluatorList predicate, string[] parameters)
        {
            throw new NotImplementedException();
        }
    }
}