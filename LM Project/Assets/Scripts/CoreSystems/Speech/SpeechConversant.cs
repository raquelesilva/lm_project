using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CoreSystems.Speech
{
	public class SpeechConversant : MonoBehaviour
	{
		// ### Desativa o di�logo se o jogador carregar com a tecla esquerda do rato
		[SerializeField] bool disappearWhenClicked = false;

		// ### Tempo total que a anima��o do fade dever� demorar
		[SerializeField] float fadeTIme = 1f;

		// ### Caso o jogador tiver que dar uma resposta neste di�logo,
		// esta var�vel pode ser usada para verificar se a resposta est� correta ou n�o
		[SerializeField] string[] correctAnswers;

		// ### Dialogo normal, que � usado sempre que este come�a
		[SerializeField, TextArea] string normalDialogue;

		// ### Caso o jogador tiver que dar uma resposta neste di�logo,
		// esta var�vel pode ser usada para o personagem dizer que a resposta do jogador est� vazia
		[SerializeField, TextArea] string answerEmptyText;

		// ### Caso o jogador tiver que dar uma resposta neste di�logo,
		// esta var�vel pode ser usada para o personagem dizer que a resposta do jogador est� errada
		[SerializeField, TextArea] string answerWasWrongText;

		// ### Caso o jogador tiver que dar uma resposta neste di�logo,
		// esta var�vel pode ser usada para o personagem dizer que a resposta do jogador est� correcta
		[SerializeField, TextArea] string answerWasCorrectText;

		// ### Se o di�logo exigir uma resposta este campo
		// tem de estar ativado para ativar a caixa de texto para o jogador inserir a sua resposta
		[SerializeField] bool hasInput = false;

		// ### Refer�ncia do objeto que cont�m o texto do di�logo
		[SerializeField] TextMeshProUGUI dialogueText;

		// ### Se no di�logo o jogador tiver que dar uma resposta,
		// esta � a caixa que dever� ser usada para esse efeito
		[SerializeField] TMP_InputField answerInput = null;

		// ### Refer�ncia do bal�o de fala (Speech Bubble)
		[SerializeField] GameObject speechBubble = null;

		// ### But�o para confirmar a resposta do jogador
		[SerializeField] Button confirmAnswerButton = null;

		// ### Refer�ncia do componente de Canvas Group que dever� de tratar do processo de fading
		[SerializeField] CanvasGroup canvasGroup = null;

		// ### Eventos que dever�o de ser executados quando o di�logo terminar
		[SerializeField] UnityEvent eventsOnClosing;

		// ### Eventos que dever�o de ser executados quandoo jogador acerta a quest�o
		[SerializeField] UnityEvent eventsOnGiveCorrectAnswer;

		// ### Verifica se est� a falar
		bool isTalking = false;

		/// <summary>
		/// Este m�todo � sempre chamado sempre que o jogo inicia ou quando este objeto se torna visivel
		/// </summary>
		private void OnEnable()
		{
			confirmAnswerButton.onClick.RemoveAllListeners();
			confirmAnswerButton.onClick.AddListener(() => ConfirmAnswer());
		}

		/// <summary>
		/// Este m�todo � sempre chamado a cada frame
		/// </summary>
		private void Update()
		{
			if (!disappearWhenClicked)
				return;

			if (!isTalking)
				return;

			if(Input.GetMouseButton(0))
				StartCoroutine(SpeechShowProcess(false));
		}

		/// <summary>
		/// Muda o di�logo normal da fala
		/// </summary>
		public void ChangeNormalDialogue(string dialogue)
		{
			normalDialogue = dialogue;

			var rt = (RectTransform)transform;
			LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
			foreach(var child in transform)
			{
				var childRT = (RectTransform)child;
				LayoutRebuilder.ForceRebuildLayoutImmediate(childRT);
			}
		}

		/// <summary>
		/// Ativa/Desativa o campo de texto que permite ao jogador dar uma resposta
		/// </summary>
		public void SetInputActive(bool hasInput)
		{
			this.hasInput = hasInput;
		}

		/// <summary>
		/// Ativa/Desativa o bal�o de fala com o processo de anima��o de fade
		/// </summary>
		public void ToggleSpeech()
		{
			StartCoroutine(SpeechShowProcess(!speechBubble.activeSelf));
		}
		
		/// <summary>
		/// Se neste di�logo o jogador tiver que dar uma resposta,
		/// ent�o este m�todo dever� ser chamado sempre que o jogador confirmar a mesma
		/// </summary>
		public void ConfirmAnswer()
		{
			HideInput();
			if (string.IsNullOrEmpty(answerInput.text) || string.IsNullOrWhiteSpace(answerInput.text))
			{
				NotifyAnswerIsEmpty();
				return;
			}

			foreach(var correctAnswer in correctAnswers)
			{
				if(correctAnswer == answerInput.text)
				{
					dialogueText.text = answerWasCorrectText;
					eventsOnGiveCorrectAnswer?.Invoke();
					return;
				}
			}

			NotifyWrongAnswer();
		}

		/// <summary>
		/// Retorna a verifica��o a verdadeiro ou falso para saber se o ator est� a falar
		/// </summary>
		public bool IsTalking()
		{
			return isTalking;
		}

		/// <summary>
		/// Di�logo que dever� de aparecer caso a resposta do jogador esteja vazia
		/// </summary>
		void NotifyAnswerIsEmpty()
		{
			dialogueText.text = answerEmptyText;
		}

		/// <summary>
		/// Di�logo que dever� de aparecer caso a resposta do jogador esteja errada
		/// </summary>
		void NotifyWrongAnswer()
		{
			dialogueText.text = answerWasWrongText;
		}
		
		/// <summary>
		/// Mostra o Input do jogador para dar a resposta (caso este di�logo fa�a uma pergunta ao jogador)
		/// </summary>
		void ShowInput()
		{
			answerInput.gameObject.SetActive(true);
			confirmAnswerButton.gameObject.SetActive(true);
			answerInput.text = string.Empty;
		}

		/// <summary>
		/// Esconde o Input do jogador para dar a resposta
		/// </summary>
		void HideInput()
		{
			answerInput.gameObject.SetActive(false);
			confirmAnswerButton.gameObject.SetActive(false);
		}

		/// <summary>
		/// L�gica para o processo de fading
		/// </summary>
		IEnumerator SpeechShowProcess(bool toShow)
		{
			if (toShow)
			{
				canvasGroup.alpha = 0;
				speechBubble.SetActive(true);
			}

			yield return new WaitForSeconds(0.1f);

			var rt = (RectTransform)transform;
			rt.ForceUpdateRectTransforms();
			foreach(Transform child in transform)
			{
				var childRT = (RectTransform)child;
				childRT.ForceUpdateRectTransforms();
			}

			yield return new WaitForSeconds(0.1f);

			float progress = 0f;
			float percentage = 0f;

			if (toShow)
			{
				dialogueText.text = normalDialogue;
				if(hasInput) ShowInput();
			}
				
			while (percentage < fadeTIme)
			{
				progress += Time.deltaTime;
				percentage = progress / fadeTIme;

				if(toShow)
					canvasGroup.alpha = percentage;
				else
					canvasGroup.alpha = 1-percentage;

				yield return null;
			}

			if (!toShow)
			{
				eventsOnClosing?.Invoke();
				speechBubble.SetActive(false);
			}

			isTalking = toShow;
		}
	}
}