using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using CoreSystems.Core;

namespace CoreSystems.Dialogues
{
    public class DialogueNode : ScriptableObject
    {
        // ### Actor principal neste di�logo
		[SerializeField, Min(1)] int overrideActor = 1;

		// ### For�a o nome do interlocutor neste Node
		[SerializeField] string overrideSpeakerName = string.Empty;

		// ### Verifica se o jogador ter� de falar neste node
		[SerializeField] bool isPlayerSpeaking = false;

        // ### Texto de di�logo do node
        [SerializeField, TextArea(1, 40)] string text;

        // ### Audio da fala deste node
        [SerializeField] AudioClip voiceAudio;

        // ### IDs (GUID) dos nodes filhos deste node
        [SerializeField] List<string> children = new List<string>();

        // ### Posi��o do retangulo deste node no editor de di�logo
        [SerializeField] Rect rectpos = new Rect(0, 0, 200, 100);

        // ### Usado para ativar/desativar a caixa de texto de reposta
        [SerializeField] bool hasInput = false;

        // ### Sempre que o jogador chegar a este node, poder� ter uma a��o que deve de ser chamada
        [SerializeField] string onEnterAction;

		// ### Sempre que o jogador sair deste node, poder� ter uma a��o que deve de ser chamada
		[SerializeField] string onExitAction;

        // ### Condi��o que este node ter� de fazer para executar algumas a��es
        [SerializeField] Condition condition;

        // ### Verficia se este node � o node principal
        [SerializeField, HideInInspector] bool isRoot = false;

        // ### Refer�ncia do texto do node
        public string Text
        {
            set
            {
                if (text != value)
                {
#if UNITY_EDITOR
                    Undo.RecordObject(this, "Update Dialog Text");
                    text = value;
                    EditorUtility.SetDirty(this);
#endif
                }

            }

            get
            {
                return text;
            }
        }

        // ### Refer�ncia do ficheiro de voz de fala
        public AudioClip VoiceAudio
        {
			set
			{
				if (voiceAudio != value)
				{
#if UNITY_EDITOR
					Undo.RecordObject(this, "Update Audio Clip");
					voiceAudio = value;
					EditorUtility.SetDirty(this);
#endif
				}

			}

			get { return voiceAudio; }
        }

        /// <summary>
        /// Retorna todos os nodes filhos
        /// </summary>
        public List<string> GetChildren()
        {
            return children;
        }

        /// <summary>
        /// Retorna o valor que indica se o jogador ter� de falar neste node
        /// </summary>
        public bool IsPlayerSpeaking()
        {
            return isPlayerSpeaking;
        }

		/// <summary>
		/// Retorna o valor que indica se � o node principal
		/// </summary>
		public bool IsRoot()
        {
            return isRoot;
        }

        /// <summary>
        /// Verifica se tem alguma caixa de texto de resposta
        /// </summary>
        public bool HasInput()
        {
            return hasInput;
        }

        /// <summary>
        /// Retorna o nome do interlocutor deste Node
        /// </summary>
		public string GetSpeakerOverride()
		{
			return overrideSpeakerName;
		}

        /// <summary>
        /// Retorna o actor que fala neste Node
        /// </summary>
		public int GetActorOverride()
		{
			return overrideActor;
		}

		/// <summary>
		/// Retorna a a��o que deve de ser executado quando o jogador entra neste node
		/// </summary>
		public string GetOnEnterAction()
        {
            Debug.Log("GetOnEnterAction");
            return onEnterAction;
        }

		/// <summary>
		/// Retorna a a��o que deve de ser executado quando o jogador sai deste node
		/// </summary>
		public string GetOnExitAction()
        {
            return onExitAction;
        }

        /// <summary>
        /// Retorna a condi��o do node
        /// </summary>
        public bool CheckCondition(IEnumerable<IPredicateEvaluator> evaluators)
        {
            return condition.Check(evaluators);
        }

#if UNITY_EDITOR
		/// <summary>
		/// Reposiciona a posi��o do node no editor
		/// </summary>
		public void SetRectPos(Vector2 _newPos)
        {
            Undo.RecordObject(this, "Move Dialog Node");
            rectpos.position = _newPos;
            EditorUtility.SetDirty(this);
        }

        /// <summary>
        /// Define o tamanho em pixeis, em comprimento e altura do retangulo do node
        /// </summary>
        public void SetRectSize(float _width, float _height)
        {
            rectpos.size = new Vector2(_width, _height);
        }

		/// <summary>
		/// Define o tamanho em pixeis, em comprimento e altura do retangulo do node
		/// </summary>
		public void SetRectSize(Vector2 _size)
        {
            rectpos.size = _size;
        }

        /// <summary>
        /// Retorna a posi��o do ret�ngulo do node no editor
        /// </summary>
        public Rect GetRectPos()
        {
            return rectpos;
        }

        /// <summary>
        /// Adiciona um novo node filho deste node
        /// </summary>
        public void AddChild(string childID)
        {
            Undo.RecordObject(this, "Dialogue Node Linked");
            children.Add(childID);
            EditorUtility.SetDirty(this);
        }

		/// <summary>
		/// Adiciona um node existente filho deste node
		/// </summary>
		public void RemoveChild(string childID)
        {
            Undo.RecordObject(this, "Dialogue Node Unlinked");
            children.Remove(childID);
            EditorUtility.SetDirty(this);
        }

        /// <summary>
        /// Define se o jogador ir� falar neste node
        /// </summary>
        public void SetPlayerSpeaking(bool _isPlayerSpeaking)
        {
            Undo.RecordObject(this, "Change Dialogue Speaker");
            isPlayerSpeaking = _isPlayerSpeaking;
            EditorUtility.SetDirty(this);
        }

        /// <summary>
        /// Define se este node passar� a ser o node principal
        /// </summary>
        public void SetRoot(bool isRoot)
        {
            Undo.RecordObject(this, "Set Root Node");
            this.isRoot = isRoot;
            EditorUtility.SetDirty(this);
        }

        /// <summary>
        /// Define se ter� o campo com caixa de texto
        /// </summary>
		public void SetHasInput(bool hasInput)
		{
            this.hasInput = hasInput;
		}

        internal bool CheckCondition(IPredicateEvaluator[] predicateEvaluators)
        {
            throw new NotImplementedException();
        }
#endif
    }
}