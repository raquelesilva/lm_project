using CoreSystems.UI.FocusManagement;
using CoreSystems.UI.MessageBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CoreSystems.UI.MessageBox.MessageBoxUI;

namespace CoreSystems.Managers
{
	public class UIManager : MonoBehaviour
	{
		// ### Este � o canvas principal, utilizado normalmente para refer�ncia quando se cria elementos de UI "run-time"
		[SerializeField] Canvas mainCanvas;

		// ### Utilizado para criar message boxes
		[SerializeField] MessageBoxUI messageBoxPrefab;

		// ### Refer�ncia do componente ChangeFocus
		ChangeFocus changeFocus;

		/// <summary>
		/// Este m�todo � sempre chamado sempre que o jogo inicia
		/// </summary>
		private void Start()
		{
			changeFocus = FindObjectOfType<ChangeFocus>();
		}

		#region Public
		/// <summary>
		/// Se n�o houver mais que uma Message Box ativa, desabilita os objetos dinamicos e elimina-os
		/// Neste momento s� as message boxes fazem a gest�o dos objetos dinamicos, mas no futuro poder� haver
		/// mais elementos que poder�o influenciar o sistema de focus
		/// </summary>
		public void TryToDisableDynamicOnFocusManager(bool activate)
		{
			var msgBoxUIs = FindObjectsOfType<MessageBoxUI>();
			if (activate && (msgBoxUIs == null || msgBoxUIs.Length < 2))
			{
				changeFocus.ResetDynamicRects();
				changeFocus.SetControlDynamic(false);
			}
		}

		/// <summary~>
		/// Facilita o processo de cria��o das "Message Boxes" ou Caixas de Mensagens
		/// </summary>
		public MessageBoxUI OpenMessageBox(string titleMessage, string message, ref List<string> buttonsNames, 
			bool hasBackgroundDisabler = false, string actionID = "", bool hasTextBox = false)
		{
			CloseMessageBoxByActionID(actionID);

			var msgBox = Instantiate(messageBoxPrefab, mainCanvas.transform);
			if (msgBox == null) return null;

			msgBox.OpenMessageBox(titleMessage, message, ref buttonsNames, changeFocus, hasBackgroundDisabler, 
				actionID, hasTextBox);

			return msgBox;
		}

		/// <summary>
		/// Retorna o canvas principal
		/// </summary>
		public Canvas GetMainCanvas()
		{
			return mainCanvas;
		}
		#endregion

		#region Private
		/// <summary>
		/// Fecha todas as Message Boxes que tenham um certo actionID
		/// </summary>
		void CloseMessageBoxByActionID(string actionID)
		{
			if (string.IsNullOrEmpty(actionID) || string.IsNullOrWhiteSpace(actionID))
				return;

			var msgBoxes = FindObjectsOfType<MessageBoxUI>();
			if (msgBoxes == null || msgBoxes.Length == 0) return;

			foreach (var msgBox in msgBoxes) 
			{
				if (msgBox == null) continue;

				var msgBoxActionID = msgBox.GetActionID();

				if (string.IsNullOrEmpty(msgBoxActionID) || string.IsNullOrWhiteSpace(msgBoxActionID))
					continue;

				if(msgBoxActionID != actionID) continue;

				msgBox.CloseMessageBox();
			}
		}
		#endregion
	}
}