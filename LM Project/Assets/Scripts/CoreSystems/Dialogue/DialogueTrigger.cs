using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace CoreSystems.Dialogues
{
	public class DialogueTrigger : MonoBehaviour
	{
		// ### Obt�m as a��es de di�logos agrupadas no inspetor e define as suas a��es
		[SerializeField] TriggerActionStruct[] dialogueActions;

		// ### Eventos a serem executados quando o jogador fecha o di�logo ou quando este termina
		[SerializeField] UnityEvent exitEvents;

		/// <summary>
		/// Quando uma a��o � chamada esta ter� de ser executada neste m�todo
		/// </summary>
		public void Trigger(string actionToTrigger)
		{
			Debug.Log("Trigger: " + actionToTrigger);
			for (int i = 0; i < dialogueActions.Length; i++)
			{
				if (actionToTrigger == dialogueActions[i].action)
				{
					dialogueActions[i].onTrigger.Invoke();
				}
			}
		}

        private void OnTriggerEnter(Collider other)
        {
			if (other.tag == "Player")
			{
				Debug.Log("I touched the player");

				string state = GetComponent<AIConversant>().GetDialogueState();
				int index = dialogueActions.ToList().FindIndex(a => a.action == state);
                dialogueActions[index].onTrigger.Invoke();
            }
        }

        /// <summary>
        /// Executa os eventos de s�ida do di�logo quando o jogador fecha ou quando este termina
        /// </summary>
        public void FinishDialogue()
		{
			exitEvents?.Invoke();
		}

		[Serializable]
		struct TriggerActionStruct
		{
			public string action;
			public UnityEvent onTrigger;
		}

        public void DestroyInteraction()
        {
            Destroy(gameObject);
        }
    }
}