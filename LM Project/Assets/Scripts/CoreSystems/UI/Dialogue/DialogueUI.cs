using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using CoreSystems.Dialogues;
using Unity.VisualScripting;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace CoreSystems.UI.Dialogues
{
    public class DialogueUI : MonoBehaviour
    {
        // ### Imagem do personagem que est� a falar
        [SerializeField] Image portrait;

        // ### Refer�ncia do texto onde cont�m o nome do ator que est� a falar
        [SerializeField] TextMeshProUGUI actorName;

        // ### Refer�ncia do texto onde cont�m o texto de di�logo
        [SerializeField] TextMeshProUGUI aiText;

        // ### Refer�ncia da caixa de texto para a resposta do jogador
        [SerializeField] TMP_InputField answerInputField;

        // ### Refer�ncia do bot�o que permite avan�ar no di�logo
        [SerializeField] Button nextButton;

        // ### Refer�ncia do bot�o que permite sair do di�logo
        [SerializeField] Button quitButton;

        // ### Refer�ncia do objeto que cont�m o texto de di�logo, nome do ator e bot�o para seguir para a frente no di�logo
        [SerializeField] GameObject aiResponse;

        // ### Refer�ncia do objeto parente que ir� conter os bot�es de escolha
        [SerializeField] Transform choiceRoot;

        // ### Refer�ncia do objeto original de bot�o de escolha
        [SerializeField] GameObject choicePrefab;

        // ### Refer�ncia do componente do jogador PlayerConversant
        PlayerConversant playerConversant;

        AudioSource audioSource;

        /// <summary>
        /// Este m�todo � sempre chamado sempre que o jogo inicia
        /// </summary>
        void Start()
        {
            playerConversant = FindObjectOfType<PlayerConversant>();
            audioSource = FindObjectOfType<PlayerConversant>().GetComponent<AudioSource>();

            playerConversant.OnConversationUpdate += UpdateUI;

            UpdateUI();
        }

        public void NextButtonPressed()
        {
            if (playerConversant.HasInput())
                playerConversant.SetAnswer(answerInputField.text);

            playerConversant.Next();
        }

        public void QuitButtonPressed()
        {
            playerConversant.Quit();
        }

        /// <summary>
        /// Este m�todo � sempre chamado sempre que o jogo termina ou quando este objeto � destruido
        /// </summary>
		private void OnDestroy()
		{
			playerConversant.OnConversationUpdate -= UpdateUI;
		}

		/// <summary>
		/// Este m�todo � sempre chamado sempre que este objeto fica invisivel
		/// </summary>
		private void OnDisable()
        {
            if (playerConversant == null) return;

            playerConversant.Quit();
        }

        /// <summary>
        /// Atualiza os textos, bot�es e imagens com base na imagem do di�logo
        /// </summary>
        void UpdateUI()
        {
            if (playerConversant == null) return;

            //Image adjustmentLayer = gameObject.GetComponent<Image>();
            //if(adjustmentLayer != null) adjustmentLayer.enabled = playerConversant.NeedAdjustmentLayer();
            gameObject.SetActive(playerConversant.IsActive());

            quitButton.gameObject.SetActive(playerConversant.CanQuitDialog());

			if (!playerConversant.IsActive()) return;

            if(actorName != null)
                actorName.text = playerConversant.GetCurrentConversantName();

            portrait.sprite = playerConversant.GetCurrentConversantPortrait();
			portrait.gameObject.SetActive(portrait.sprite != null);

            if (playerConversant.GetAudio() != null)
            {
                audioSource.clip = playerConversant.GetAudio();
                audioSource.Play();
            }
            
            if (aiResponse != null)
			    aiResponse.SetActive(!playerConversant.IsChoosing());

            choiceRoot.gameObject.SetActive(playerConversant.IsChoosing());

            if (playerConversant.IsChoosing())
            {
                BuildChoiceList();
            }
            else
            {
                aiText.text = playerConversant.GetText();
                nextButton.gameObject.SetActive(playerConversant.HasNext());
                quitButton.gameObject.SetActive(!playerConversant.HasNext());
            }

			var nextText = nextButton.GetComponentInChildren<TextMeshProUGUI>();
            if (playerConversant.HasInput())
            {
                answerInputField.gameObject.SetActive(true);
                if (nextText != null)
                    nextText.text = "Confirmar";
            }
            else
            {
                answerInputField.gameObject.SetActive(false);
                if (nextText != null)
                    nextText.text = "Pr�ximo";
            }
        }

        /// <summary>
        /// Cria os bot�es de escolha do jogador
        /// </summary>
        private void BuildChoiceList()
        {
            // Destroy any choice buttons if there is any
            foreach (Transform trans in choiceRoot)
            {
                Destroy(trans.gameObject);
            }

            foreach (DialogueNode choice in playerConversant.GetChoices())
            {
                GameObject choiceInstance = Instantiate(choicePrefab, choiceRoot);
                var textComp = choiceInstance.GetComponentInChildren<TextMeshProUGUI>();
                textComp.text = choice.Text;

                Button button = choiceInstance.GetComponentInChildren<Button>();
                button.onClick.AddListener(() =>
                {
                    playerConversant.SelectChoice(choice);
                });
            }
        }
    }
}