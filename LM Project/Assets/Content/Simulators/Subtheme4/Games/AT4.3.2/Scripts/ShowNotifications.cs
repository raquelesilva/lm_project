using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowNotifications : MonoBehaviour
{
    [Header("INSTANCE")]
    public static ShowNotifications Instance;

    [Header("REFERÊNCIAS DOS OBJETOS")]
    [SerializeField] private GameObject shortNotifObject;
    [SerializeField] private TextMeshProUGUI shortNotifText;
    [SerializeField] private GameObject activityMessageObject;
    [SerializeField] private TextMeshProUGUI activityMessageText;

    private void Awake()
    {
        Instance = this;
    }

    public void ShowShortNotification(string message)
    {
        OpenWindow(shortNotifObject);
        shortNotifText.text = message;

        StartCoroutine(CloseWindow(shortNotifObject));
    }

    public void ShowActivityMessage(string message)
    {
        OpenWindow(activityMessageObject);
        activityMessageText.text = message;

        StartCoroutine(CloseWindow(activityMessageObject));
    }

    private IEnumerator CloseWindow(GameObject window)
    {
        yield return new WaitForSeconds(4f);

        window.SetActive(false);
    }

    private void OpenWindow(GameObject window)
    {
        shortNotifObject.SetActive(false);
        activityMessageObject.SetActive(false);

        window.SetActive(true);
    }
}