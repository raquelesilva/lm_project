using CoreSystems.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreSystems.Dialogues
{
	public class AIConversant : MonoBehaviour, IPredicateEvaluator
    {
		// ### Refer�ncia do di�logo associado
		[SerializeField] Dialogue dialogue = null;

		// ### Estado atual do di�logo
		[SerializeField] string dialogueState = "default";

		// ### Nome do ator
		[SerializeField] string conversantName = "Nobody";

		[SerializeField] bool darkenBackground = true;
		[SerializeField] bool playerCanQuitDialog = true;
		// ### Refer�ncia do componente PlayerConversant do jogador
		private PlayerConversant playerConversant;

		/// <summary>
		/// Este m�todo � sempre chamado sempre que o jogo inicia
		/// </summary>
		private IEnumerator Start()
		{
			yield return new WaitForSeconds(0.1f);

			playerConversant = FindObjectOfType<PlayerConversant>();
		}

		/// <summary>
		/// Come�a o di�logo com este ator
		/// </summary>
		public void StartDialogue()
		{
			if (dialogue == null) { return; }
			if (playerConversant == null) { playerConversant = FindObjectOfType<PlayerConversant>(); }

			playerConversant.StartDialogue(this, dialogue, darkenBackground, playerCanQuitDialog);
		}

		/// <summary>
		/// Define o estado de di�logo
		/// </summary>
		public void SetDialogueState(string dialogueState)
		{
			this.dialogueState = dialogueState;
		}

		/// <summary>
		/// Define o di�logo que deve de usar
		/// </summary>
		public void SetDialogue(Dialogue dialogue)
		{
			this.dialogue = dialogue;
		}

		/// <summary>
		/// Retorna o estado de di�logo
		/// </summary>
		public string GetDialogueState()
		{
			return dialogueState;
		}

		/// <summary>
		/// Retorna o di�logo atual
		/// </summary>
		public Dialogue GetDialogue()
		{
			return dialogue;
		}

		/// <summary>
		/// Retorna o nome do ator
		/// </summary>
		public string GetName()
		{
			return conversantName;
		}

		/// <summary>
		/// Avalia a condi��o do estado de di�logo
		/// </summary>
		public bool? Evaluate(EvaluatorList predicate, string[] parameters)
		{
			switch (predicate)
			{
				case EvaluatorList.IsOnDialogueState:
					{
						string diagState = GetDialogueState();
						diagState = diagState.Replace(" ", "");
						diagState = diagState.ToLower();

						return diagState == parameters[0].Replace(" ", "");
					}
				case EvaluatorList.HasInputText:
					{
						var inputAnswer = playerConversant.GetAnswer();

						if (string.IsNullOrEmpty(inputAnswer) || string.IsNullOrWhiteSpace(inputAnswer))
							return true;

						inputAnswer = inputAnswer.ToLower();
						return inputAnswer == parameters[0];
					}
				default: return null;
			}
		}

		/// <summary>
		/// Este m�todo � usado para guardar no ficheiro de grava��o 
		/// o nome do di�logo usado e o estado do mesmo
		/// </summary>
		public object CaptureState()
		{
			var state = new AIConversantRecord();

			state.dialogueName = dialogue.name;
			state.dialogueState = dialogueState;

			return state;
		}

		/// <summary>
		/// Carrega o di�logo atual e o estado do mesmo do ficheiro de grava��o
		/// </summary>
		public void RestoreState(object _state)
		{
			AIConversantRecord dialogueRecord = (AIConversantRecord)_state;

			var savedDialogue = Resources.LoadAll<Dialogue>("");
			foreach (Dialogue diag in savedDialogue)
			{
				if (diag.name == dialogueRecord.dialogueName)
				{
					dialogue = diag;
				}
			}

			dialogueState = dialogueRecord.dialogueState;
		}

		[Serializable]
		struct AIConversantRecord
		{
			public string dialogueName;
			public string dialogueState;
		}
	}
}