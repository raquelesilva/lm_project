using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CoreSystems.UI.Utils
{
	public class TextColorChanger : MonoBehaviour
	{
		[SerializeField] TextMeshProUGUI text = null;
		[SerializeField] Image image = null;
		[SerializeField] Color colorToChange = Color.white;

		Color originalColor = Color.white;

		private void Start()
		{
			if (text != null) originalColor = text.color;
			if (image != null) originalColor = image.color;
		}

		public void ChangeColor()
		{
			if (text != null) text.color = colorToChange;
			if (image != null) image.color = colorToChange;
		}

		public void ResetColor()
		{
			if (text != null) text.color = originalColor;
			if (image != null) image.color = originalColor;
		}
	}
}
