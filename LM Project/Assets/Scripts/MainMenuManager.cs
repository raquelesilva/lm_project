using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] GameObject soundSettings;
    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject noSoundImg;

    public bool isSoundOn = true;

    public void StartMainMenu()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void OpenSoundSettings()
    {
        soundSettings.SetActive(true);
    }

    public void CloseSoundSettings()
    {
        soundSettings.SetActive(false);
    }

    public void LeaveGame()
    {
        Debug.Log("LeaveGame");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    public void ChangeVolume(Slider volumeSlider)
    {
        Debug.Log("ChangeValue: " + volumeSlider.value);
        
        Debug.Log("audiomanager: " + audioSource.volume);
        audioSource.volume = volumeSlider.value;
    }

    public void ToggleSound()
    {
        isSoundOn = !isSoundOn;

        if (!isSoundOn)
        {
            noSoundImg.SetActive(true);
        }
    }
}