using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreSystems.Dialogues
{
	[CreateAssetMenu(fileName = "New Dialogue Actor", menuName = "Dialogue/Actor", order = 0)]
	public class DialogueActor : ScriptableObject
	{
		// ### Nome do actor que est� a falar
		[SerializeField] string actorName;

		// ### Imagem do actor que est� a falar
		[SerializeField] Sprite actorPortrait;

		/// <summary>
		/// Define o nome do actor
		/// </summary>
		public void SetName(string actorName)
		{
			this.actorName = actorName;
		}

		/// <summary>
		/// Define a imagem do actor
		/// </summary>
		public void SetPortrait(Sprite actorPortrait)
		{
			this.actorPortrait = actorPortrait;
		}

		/// <summary>
		/// Retorna a imagem do actor
		/// </summary>
		public Sprite GetPortrait()
		{
			return actorPortrait;
		}

		/// <summary>
		/// Retorna o nome do actor
		/// </summary>
		public string GetName()
		{
			return actorName;
		}
	}
}